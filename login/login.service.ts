import { Injectable }              from '@angular/core';
import {HttpModule, Http,Response} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import * as myGlobals from '../shared/globals';


@Injectable()
export class LoginService
{
    http: Http;
    returnCommentStatus:Object = [];
    constructor(public _http: Http)
    {
        this.http = _http;
    }

    login(value: any)
    {
      let form = {
         'username' : value.username,
         'password' : value.password
      }
      let body = JSON.stringify(form);
      let dataa = 'data='+body;
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded;charset=UTF-8');
      return this.http.post(myGlobals.baseUrl+'api/login/',dataa, { headers }).map(
            (res: Response) => res.json() || {});
    }

}

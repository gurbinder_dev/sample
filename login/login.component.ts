import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';
import {HttpModule, Http,Response} from '@angular/http';
import { LoginService }    from './login.service';
import {ToasterModule, ToasterService} from 'angular2-toaster';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  private toasterService: ToasterService;

login : FormGroup;
http: Http;

  constructor(  fb: FormBuilder , public _http: Http , private _loginservice: LoginService , private router: Router , toasterService: ToasterService)
  {
      this.toasterService = toasterService;
      this.http = _http;
      this.login = fb.group({
      'username' : [null,Validators.required],
      'password': [null,Validators.required]
    })
  }

  responseStatus:Object= [];
  submitForm(value: any)
  {
    this._loginservice.login(value).subscribe(
      data => {
        if(data.success)
        {
            console.log( this.responseStatus = data );
            this.toasterService.pop('success', data.success +' Redirecting...', '' );
            let body = JSON.parse ( JSON.stringify(data.data) );
            let tkn = JSON.stringify(body[0]);
            localStorage.setItem('dbcse_token', tkn);
            setTimeout((router: Router) => {
                this.router.navigate(['/dashboard']);
            }, 2000);  //5s
        }
        else
        {
          this.responseStatus = {'error': data.error}
        }
      },
      err => {
        this.responseStatus = {'error':'Unknown Error occured , try again later'}
    },
      () => console.log('Submitted Successfully')
   );
  }

  hidemsg(event: any)
  {
    this.responseStatus = [];
  }

  ngOnInit() {

  }

}

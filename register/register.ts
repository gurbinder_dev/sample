export class Register {

  constructor(
    public com_name: string,
    public user_name: string,
    public com_email: string,
    public com_phone: string,
    public user_password: string
  )

  {  }

}

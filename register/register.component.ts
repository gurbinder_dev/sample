import { Component,OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AbstractControl, FormArray, FormControl, FormBuilder, FormGroup , Validators} from '@angular/forms';
import { Register }    from './register';
import { NgForm   } from '@angular/forms';
import { RegisterService }    from './register.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterModule, ToasterService} from 'angular2-toaster';

function passwordMatcher( c : AbstractControl )
{
	console.log(c.get('user_password').value);
	return c.get('user_password').value === c.get('user_password_rpt').value
		? null : { 'nomatch' : true };
}




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [RegisterService]
})

export class RegisterComponent implements OnInit {



  private toasterService: ToasterService;

  signup 	: FormGroup;
	responseStatus:Object= [];
	submitAttempt: boolean = false;
	public showhidemsg	   = false;

    constructor(  public fb: FormBuilder  , private _registerservice: RegisterService , private router: Router , toasterService: ToasterService)
    {
  		this.toasterService = toasterService;

  		this.signup = this.fb.group({
  			'com_name' 			: ['',[Validators.required, Validators.minLength(5)]],
        'user_name' 			: ['',[Validators.required,Validators.minLength(4)]],
        'com_phone' 			: ['',[Validators.required,Validators.pattern('[0-9]*'), Validators.minLength(10)]],
  			'com_email' 		  	: ['',[Validators.required, Validators.email]],
  			account : this.fb.group({
  				user_password : ['',[Validators.required,Validators.minLength(6)]],
  				user_password_rpt : ['',[Validators.required,Validators.minLength(6)]],
  			},
  			{validator:passwordMatcher})
  		});

    }

    @Input() data:Register;
    status:boolean ;


    model = new Register('','','','','');
    submitted = false;

    onSubmit(signup: any )
    {
        console.log( signup );
        this.submitAttempt = true;
    		if(!this.signup.valid)
    			return false;
      		this._registerservice.adduser(signup).subscribe(
        data => {
          this.showhidemsg = true;
  				setTimeout(function() {
  					this.showhidemsg = false;
  				}.bind(this), 3000);
  				this.responseStatus = [];
          if(data.success)
          {
            this.toasterService.pop('success', data.success +' Redirecting...', '' );
            let body = JSON.parse ( JSON.stringify(data.data) );
            // console.log( body[0].COM_NAME );
            let tkn = JSON.stringify(body[0]);
            localStorage.setItem('dbcse_token', tkn);
            this.signup.reset();
  					this.submitAttempt = false;
            setTimeout((router: Router) => {
                this.router.navigate(['/dashboard']);
            }, 2000);  //5s
          }
					else
					this.responseStatus = data;

        },
        err => console.log(err),
        () => console.log('Submitted Successfully')
     );
     this.status = true;
     this.submitted = true;
  }
 ngOnInit()
 {

 }

}

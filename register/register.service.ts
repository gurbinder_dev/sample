import { Injectable }              from '@angular/core';
import {HttpModule, Http,Response} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Register }    from './register';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import * as myGlobals from '../shared/globals';


@Injectable()
export class RegisterService
{
    http: Http;
    returnCommentStatus:Object = [];
    constructor(public _http: Http)
    {
        this.http = _http;
    }


    adduser(data:Register)
    {
      let body = JSON.stringify(data);
      let dataa = 'data='+body;
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded;charset=UTF-8');
      return this.http.post(myGlobals.baseUrl+'api/create-employee/',dataa, { headers }).map(
            (res: Response) => res.json() || {});
    }

}
